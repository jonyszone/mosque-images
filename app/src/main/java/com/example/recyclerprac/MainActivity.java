package com.example.recyclerprac;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.RecyclerViewAccessibilityDelegate;

import android.os.Bundle;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> imageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initImageBitmaps();
    }

    private void initImageBitmaps() {

        imageUrls.add("https://images.unsplash.com/photo-1540866225557-9e4c58100c67?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=995&q=80");
        names.add("Madina: Al Masjid An Nabawi");

        imageUrls.add("https://images.unsplash.com/photo-1590075865003-e48277faa558?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80");
        names.add("Mosque: Sheikh Zayed Grand Mosque");

        imageUrls.add("https://images.unsplash.com/photo-1527246574940-ebf9c5ffd9a9?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1025&q=80");
        names.add("Abu Dhabi");

        imageUrls.add("https://images.unsplash.com/photo-1592526074440-36fd2c83ec08?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80");
        names.add("Grand Mosque in Abu Dhabi");

        imageUrls.add("https://images.unsplash.com/photo-1590075865003-e48277faa558?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80");
        names.add("The Sheik Zayed Grand Mosque");

        imageUrls.add("https://images.unsplash.com/photo-1596911942922-521ec0db907d?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80");
        names.add(" walkway of the Sheik Zayed Grand Mosque");

        initRecyclerView();

    }

    private void initRecyclerView() {

        RecyclerView recyclerView = findViewById(R.id.recycler_image);
        GalleryAdapter galleryAdapter = new GalleryAdapter(this, names, imageUrls);
        recyclerView.setAdapter(galleryAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}