package com.example.recyclerprac;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {

    private final ArrayList<String> imageNames;
    private final ArrayList<String> imageLists;
    private final Context mContext;

    public GalleryAdapter(Context context, ArrayList<String> imageNamesList, ArrayList<String> imageList) {

        imageNames = imageNamesList;
        imageLists = imageList;
        mContext = context;
    }

    @NonNull
    @Override
    public GalleryAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GalleryAdapter.ViewHolder holder, int position) {

        Glide.with(mContext)
                .asBitmap()
                .load(imageLists.get(position))
                .into(holder.image);

        holder.imageName.setText(imageNames.get(position));
        holder.parentLayout.setOnClickListener(view -> {
            Toast.makeText(mContext, imageNames.get(position), Toast.LENGTH_SHORT).show();

            Intent intent = new Intent(mContext, GalleryActivity.class);
            intent.putExtra("image_url", imageLists.get(position));
            intent.putExtra("image_name", imageNames.get(position));

            mContext.startActivity(intent);

        });


    }

    @Override
    public int getItemCount() {
        return imageLists.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        CircleImageView image;
        TextView imageName;
        RelativeLayout parentLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            imageName = itemView.findViewById(R.id.image_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);

        }
    }
}